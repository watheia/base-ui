import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { MDXLayout } from "./mdx-layout"
// @ts-ignore
import MdxContentDefault from "./mdx-layout.docs.md"
// @ts-ignore
import MdxContentExample from "./md-example.md"

export const MDXLayoutExample = () => (
  <ThemeProvider>
    <MDXLayout>
      <MdxContentDefault />
    </MDXLayout>
  </ThemeProvider>
)

export const MDXLayoutSecondExample = () => (
  <ThemeProvider>
    <MDXLayout>
      <MdxContentExample />
    </MDXLayout>
  </ThemeProvider>
)

MDXLayoutSecondExample.canvas = {
  overflow: "scroll",
  height: 200,
}
