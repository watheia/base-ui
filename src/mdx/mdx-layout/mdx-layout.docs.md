---
labels: ["react", "typescript", "mdx", "docs", "ui"]
description: "MDX Provider that uses Watheia’s “documenter” design system."
---

The MDX layout is an MDX Provider that uses the Documenter design system to give your docs a
look-and-feel that is similar to the Workspace/Scope UI.
