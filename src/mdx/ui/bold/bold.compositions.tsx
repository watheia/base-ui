import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Bold } from "./bold"

export const BoldExample = () => {
  return (
    <ThemeProvider>
      <Bold>bold text</Bold>
    </ThemeProvider>
  )
}
