import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Ul } from "./ul"

export const UlExample = () => {
  return (
    <ThemeProvider>
      <Ul>
        <li>Ul text</li>
        <li>Ul text</li>
        <li>Ul text</li>
      </Ul>
    </ThemeProvider>
  )
}
