import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Italic } from "./italic"

export const ItalicExample = () => {
  return (
    <ThemeProvider>
      <Italic>Italic text</Italic>
    </ThemeProvider>
  )
}
