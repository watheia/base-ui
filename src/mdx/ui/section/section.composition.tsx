import React from "react"
import { Section } from "./section"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

export const Xxs = () => {
  return (
    <ThemeProvider>
      <Section>section content</Section>
    </ThemeProvider>
  )
}
