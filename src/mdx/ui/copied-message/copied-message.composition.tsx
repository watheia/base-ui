import React, { useState } from "react"
import { CopiedMessage } from "./copied-message"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Icon } from "@watheia/base-ui.atoms.icon"
import { H5 } from "@watheia/base-ui.mdx.ui.heading"

const codeString = `export function Anchor(props: AnchorProps) {
  return (
    <a {...props} id={props.href} href={href} className={styles.anchor}>
      <Icon of="anchor" className={styles.anchorIcon}></Icon>
    </a>
  );
}`

export const CopiedMessageExample = () => {
  return (
    <ThemeProvider>
      <div style={{ position: "relative" }}>
        <CopiedMessage show={true} />
      </div>
    </ThemeProvider>
  )
}

export const HiddenCopiedMessageExample = () => {
  const [isCopied, setIsCopied] = useState(false)

  const handleClick = () => {
    setIsCopied(true)
    setTimeout(() => setIsCopied(false), 2000)
  }
  return (
    <ThemeProvider>
      <div style={{ position: "relative", width: 200 }}>
        <H5>Click icon to copy</H5>
        <Icon onClick={handleClick} of="copy-cmp" />
        <CopiedMessage show={isCopied} style={{ top: 23, right: 120 }} />
      </div>
    </ThemeProvider>
  )
}
