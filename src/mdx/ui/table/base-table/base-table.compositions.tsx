import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Td } from "@watheia/base-ui.mdx.ui.table.td"

import { Tr } from "@watheia/base-ui.mdx.ui.table.tr"
import { Table } from "./base-table"

export const TableExample = () => {
  return (
    <ThemeProvider>
      <Table>
        <Tr>
          <th>title1</th>
          <th>title2</th>
          <th>title3</th>
        </Tr>
        <Tr>
          <Td>cell</Td>
          <Td>cell</Td>
          <Td>cell</Td>
        </Tr>
        <Tr>
          <Td>cell</Td>
          <Td>cell</Td>
          <Td>cell</Td>
        </Tr>
      </Table>
    </ThemeProvider>
  )
}
