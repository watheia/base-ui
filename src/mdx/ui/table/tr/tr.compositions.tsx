import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Td } from "@watheia/base-ui.mdx.ui.table.td"

import { Tr } from "./tr"
export const TrExample = () => {
  return (
    <ThemeProvider>
      <Tr>
        <Td>Tr text</Td>
        <Td>Tr text</Td>
        <Td>Tr text</Td>
      </Tr>
    </ThemeProvider>
  )
}
