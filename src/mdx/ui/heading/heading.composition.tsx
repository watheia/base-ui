import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { H1, H2, H3, H4, H5, H6 } from "./heading"

export const HeadingXl = () => {
  return (
    <ThemeProvider>
      <H1 style={{ whiteSpace: "nowrap" }}>main header</H1>
    </ThemeProvider>
  )
}
export const HeadingLg = () => (
  <ThemeProvider>
    <H2 style={{ whiteSpace: "nowrap" }}>main header</H2>
  </ThemeProvider>
)
export const HeadingMd = () => (
  <ThemeProvider>
    <H3 style={{ whiteSpace: "nowrap" }}>main header</H3>
  </ThemeProvider>
)
export const HeadingSm = () => (
  <ThemeProvider>
    <H4 style={{ whiteSpace: "nowrap" }}>main header</H4>
  </ThemeProvider>
)
export const HeadingXs = () => (
  <ThemeProvider>
    <H5 style={{ whiteSpace: "nowrap" }}>main header</H5>
  </ThemeProvider>
)
export const HeadingXxs = () => (
  <ThemeProvider>
    <H6 style={{ whiteSpace: "nowrap" }}>main header</H6>
  </ThemeProvider>
)
