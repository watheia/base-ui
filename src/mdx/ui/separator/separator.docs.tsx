import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Separator } from "./separator"
import { Section } from "@watheia/base-ui.mdx.ui.section"

export const labels = ["react", "ui-component", "layout"]

export const examples = [
  {
    scope: {
      Separator,
      Section,
      ThemeProvider,
    },
    title: "Using the Seperator component.",
    code: `
<ThemeProvider>

    <Section>
        SECTION A
    </Section>

    <Separator />

    <Section>
        SECTION B
    </Section>

</ThemeProvider>
`,
  },
]
