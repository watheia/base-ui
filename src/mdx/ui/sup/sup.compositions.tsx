import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Sup } from "./sup"

export const SupExample = () => {
  return (
    <ThemeProvider>
      <>
        <Sup>superscript text example</Sup> next to regular text
      </>
    </ThemeProvider>
  )
}
