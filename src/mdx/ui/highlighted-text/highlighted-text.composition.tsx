import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { HighlightedText } from "./highlighted-text"

export const LargeText = () => {
  return (
    <ThemeProvider>
      <HighlightedText element="p" size="lg">
        Large highlighted text.
      </HighlightedText>
    </ThemeProvider>
  )
}

export function SmallText() {
  return (
    <ThemeProvider>
      <HighlightedText element="p" size="sm">
        Small highlighted text.
      </HighlightedText>
    </ThemeProvider>
  )
}
