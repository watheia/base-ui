import React from "react"
import { Paragraph } from "./paragraph"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

// tODO - make text stay single line

export const ParagraphXxl = () => {
  return (
    <ThemeProvider>
      <div>
        <Paragraph size="xxl">xxl text</Paragraph>
      </div>
    </ThemeProvider>
  )
}
export const ParagraphXl = () => {
  return (
    <ThemeProvider>
      <div>
        <Paragraph size="xl">xl text</Paragraph>
      </div>
    </ThemeProvider>
  )
}
export const ParagraphLg = () => {
  return (
    <ThemeProvider>
      <div>
        <Paragraph size="lg">lg text</Paragraph>
      </div>
    </ThemeProvider>
  )
}
export const ParagraphMd = () => {
  return (
    <ThemeProvider>
      <div>
        <Paragraph size="md">md text</Paragraph>
      </div>
    </ThemeProvider>
  )
}
export const ParagraphSm = () => {
  return (
    <ThemeProvider>
      <div>
        <Paragraph size="sm">sm text</Paragraph>
      </div>
    </ThemeProvider>
  )
}
export const ParagraphXs = () => {
  return (
    <ThemeProvider>
      <div>
        <Paragraph size="xs">xs text</Paragraph>
      </div>
    </ThemeProvider>
  )
}

export const ParagraphXxs = () => {
  return (
    <ThemeProvider>
      <div>
        <Paragraph size="xxs">xxs text</Paragraph>
      </div>
    </ThemeProvider>
  )
}
