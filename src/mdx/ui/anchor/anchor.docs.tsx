import { Anchor } from "./anchor"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { H5 } from "@watheia/base-ui.mdx.ui.heading"

export const labels = ["react", "ui-component", "link"]

export const examples = [
  {
    scope: {
      Anchor,
      H5,
      ThemeProvider,
    },
    title: "Using the Anchor component",
    description: `The Anchor component can only link to itself.
Place it to either side of the title you wish to point to, so as to have it scroll to the same vertical point on the page.`,
    code: `
<ThemeProvider>
    <div style={{display: 'inline-flex', alignItems: 'center'}}>
        <H5 style={{marginRight: '10px'}}>A title with a link</H5>
        <Anchor href="this-example" />
    </div>
</ThemeProvider>
`,
  },
]
