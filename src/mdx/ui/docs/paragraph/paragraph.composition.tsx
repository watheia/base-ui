import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { P } from "./paragraph"

export const ParagraphExample = () => (
  <ThemeProvider>
    <P data-testid="test-p">p element</P>
  </ThemeProvider>
)
