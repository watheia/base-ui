import React, { HTMLAttributes } from "react"
import { HighlightedText } from "@watheia/base-ui.mdx.ui.highlighted-text"

export function HighlightedTextSpan({
  children,
  className,
  ...rest
}: HTMLAttributes<HTMLElement>) {
  return (
    <HighlightedText {...rest} className={className} element="span" size="xs">
      {children}
    </HighlightedText>
  )
}
