import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { HighlightedTextSpan } from "./highlighted-text-span"

export const HighlightedTextSpanExample = () => {
  return (
    <ThemeProvider>
      <HighlightedTextSpan data-testid="test-span">Highlighted Text</HighlightedTextSpan>
    </ThemeProvider>
  )
}
