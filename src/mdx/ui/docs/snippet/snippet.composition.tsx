import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Snippet } from "./snippet"

const code = `
<div>simple div element</div>
`

export const SnippetExample = () => {
  return (
    <ThemeProvider>
      <Snippet>{code}</Snippet>
    </ThemeProvider>
  )
}

export const SnippetLiveExample = () => {
  return (
    <ThemeProvider>
      <Snippet live>{code}</Snippet>
    </ThemeProvider>
  )
}
