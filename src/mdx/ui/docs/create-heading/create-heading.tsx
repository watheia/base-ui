import React, { HTMLAttributes } from "react"
import classnames from "classnames"
import type { Sizes } from "@watheia/base-ui.mdx.ui.heading"
import { LinkedHeading } from "@watheia/base-ui.mdx.ui.linked-heading"
import styles from "./create-heading.module.scss"

export function createHeading(size: Sizes) {
  return function Heading({
    children,
    className,
    ...rest
  }: HTMLAttributes<HTMLHeadingElement>) {
    const isMainHeading = size === "lg" || size === "md"
    return (
      <LinkedHeading
        {...rest}
        className={classnames(
          className,
          styles.mdxLinkedHeading,
          isMainHeading && styles.mainHeadingStyles
        )}
        size={size}
      >
        {children}
      </LinkedHeading>
    )
  }
}
