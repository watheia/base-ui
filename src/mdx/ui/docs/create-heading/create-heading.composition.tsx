import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { createHeading } from "./create-heading"

export const CreateH1Example = () => {
  const Heading = createHeading("lg")
  return (
    <ThemeProvider>
      <Heading data-testid="test-create-heading">H1 size</Heading>
    </ThemeProvider>
  )
}

export const CreateH2Example = () => {
  const Heading = createHeading("md")
  return (
    <ThemeProvider>
      <Heading data-testid="test-create-heading">H2 size</Heading>
    </ThemeProvider>
  )
}

export const CreateH3Example = () => {
  const Heading = createHeading("sm")
  return (
    <ThemeProvider>
      <Heading data-testid="test-create-heading">H3 size</Heading>
    </ThemeProvider>
  )
}

export const CreateH4Example = () => {
  const Heading = createHeading("xs")
  return (
    <ThemeProvider>
      <Heading data-testid="test-create-heading">H4 size</Heading>
    </ThemeProvider>
  )
}

export const CreateH5Example = () => {
  const Heading = createHeading("xxs")
  return (
    <ThemeProvider>
      <Heading data-testid="test-create-heading">H5 size</Heading>
    </ThemeProvider>
  )
}

export const CreateH6Example = () => {
  const Heading = createHeading("xxs")
  return (
    <ThemeProvider>
      <Heading data-testid="test-create-heading">H6 size</Heading>
    </ThemeProvider>
  )
}
