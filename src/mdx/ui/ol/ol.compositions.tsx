import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Ol } from "./ol"

export const OlExample = () => {
  return (
    <ThemeProvider>
      <Ol>
        <li>Ol text</li>
        <li>Ol text</li>
        <li>Ol text</li>
      </Ol>
    </ThemeProvider>
  )
}
