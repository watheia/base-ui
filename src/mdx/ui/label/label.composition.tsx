import React from "react"
import { Label } from "./label"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

export const LightLabel = () => {
  return (
    <ThemeProvider>
      <Label>light label</Label>
    </ThemeProvider>
  )
}

export const DarkLabel = () => {
  return (
    <ThemeProvider>
      <Label style={{ backgroundColor: "#414141", color: "white" }}>dark label</Label>
    </ThemeProvider>
  )
}

const List = () => {
  return (
    <ul>
      <li>First</li>
      <li>Second</li>
      <li>Third</li>
    </ul>
  )
}
