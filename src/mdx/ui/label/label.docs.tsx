export const labels = ["react", "ui-component", "label"]

import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Label } from "./label"

export const examples = [
  {
    scope: {
      Label,
      ThemeProvider,
    },
    title: "Using the Label component",
    code: `
<ThemeProvider>
    <Label>
        Label
    </Label>
</ThemeProvider>
`,
  },
  {
    scope: {
      Label,
      ThemeProvider,
    },
    description: "Customizing styling",
    code: `
<ThemeProvider>
    <Label style={{backgroundColor: '#414141', color: 'white'}}>
        Dark Label
    </Label>
</ThemeProvider>
`,
  },
]
