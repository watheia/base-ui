import React from "react"
import { CodeSnippet } from "./code-snippet"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

const codeString = `export function Anchor(props: AnchorProps) {
  return (
    <a {...props} id={props.href} href={href} className={styles.anchor}>
      <Icon of="anchor" className={styles.anchorIcon}></Icon>
    </a>
  );
}`

export const CodeSnippetExample = () => {
  return (
    <ThemeProvider>
      <CodeSnippet>{codeString}</CodeSnippet>
    </ThemeProvider>
  )
}
