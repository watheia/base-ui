import React from "react"
import { Playground } from "./react-playground"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

const codeString = `<div>hello playground</div>`

const functionString = `
() => {
  const Button = ({ children }) => (
      <button >{children}</button>
  );
  const scope = { Button };
  const code = "<Button>my first playground button</Button>";

  return <button>Click Here</button>
}`

export const HelloWorld = () => {
  return (
    <ThemeProvider>
      <div style={{ width: "400px" }}>
        <Playground code={codeString} />
      </div>
    </ThemeProvider>
  )
}
export const FunctionExmple = () => {
  return (
    <ThemeProvider>
      <div style={{ width: "600px" }}>
        <Playground code={functionString} />
      </div>
    </ThemeProvider>
  )
}
