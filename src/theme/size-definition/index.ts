import styles from "./size-definition.module.scss"
const { headingFontSize, textFontSize, heading } = styles
export { headingFontSize, textFontSize, heading }
