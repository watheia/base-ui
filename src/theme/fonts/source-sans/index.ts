export * from "./source-sans"

import styles from "./source-sans.module.scss"
const { sourceSansFont } = styles
export { sourceSansFont }
