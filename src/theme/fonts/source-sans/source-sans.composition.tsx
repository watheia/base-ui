import React from "react"
import { SourceSans, sourceSansFont } from "./index"
import classNames from "classnames"

/**
 * @name SourceSansFont
 * @description
 * Applies shared styles to all child components.
 *
 * This includes:
 * - Colors
 * - Headers and paragraphs font-size, margins, etc
 * - Brand font
 * - Shadows
 * - Specific brand related styles
 *
 * @example
 * <ThemeProvider>
 *  <Paragraph>I got all the base styles! yippee!</Paragraph>
 * </ThemeProvider>
 */
export const SourceSansFont = () => (
  <>
    <SourceSans />
    <div className={classNames(sourceSansFont)}>
      <span>This is a simple text.</span>
    </div>
  </>
)

export const WithoutFont = () => (
  <div>
    <span>This is a simple text.</span>
  </div>
)
