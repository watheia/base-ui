import { createContext } from "react"

export type ThemeContextType = {
  /**
   * primary color of theme.
   */
  mode?: "light" | "dark"
}

export const ThemeContext = createContext<ThemeContextType>({
  mode: "light",
})
