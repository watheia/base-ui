import { CssBaseline, ThemeProvider as MuiThemeProvider } from "@material-ui/core"
import { brands } from "@watheia/base-ui.theme.brand-definition"
import { primaryPalette } from "@watheia/base-ui.theme.color-definition"
import { IconFont } from "@watheia/base-ui.theme.fonts.icon-font"
import { Roboto } from "@watheia/base-ui.theme.fonts.roboto"
import { SourceSans, sourceSansFont } from "@watheia/base-ui.theme.fonts.source-sans"
import { headingMargins } from "@watheia/base-ui.theme.heading-margin-definition"
import { shadowTheme } from "@watheia/base-ui.theme.shadow-definition"
import { headingFontSize, textFontSize } from "@watheia/base-ui.theme.size-definition"
import classNames from "classnames"
import React from "react"
import createTheme from "./theme"
import { ThemeContext } from "./theme-context"

const ICON_MOON_VERSION = "mxd7i0"

export type ThemeOptions = {
  /**
   * primary color mode of theme.
   */
  mode?: "light" | "dark"
} & React.HTMLAttributes<HTMLDivElement>

export function ThemeProvider({ children, mode = "light", ...props }: ThemeOptions) {
  const theme = createTheme(mode)
  return (
    <ThemeContext.Provider value={{ mode }}>
      <CssBaseline />
      <Roboto />
      <SourceSans />
      <IconFont query={ICON_MOON_VERSION} />
      <div
        {...props}
        className={classNames(
          headingFontSize,
          textFontSize,
          sourceSansFont,
          shadowTheme,
          primaryPalette,
          brands,
          headingMargins,
          props.className
        )}
      >
        <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>
      </div>
    </ThemeContext.Provider>
  )
}
