import React from "react"
import { render } from "@testing-library/react"
import { DefaultPaper } from "./paper.composition"

it("should render with the correct text", () => {
  const { getByText } = render(<DefaultPaper />)
  const rendered = getByText("Hello, World!")
  expect(rendered).toBeTruthy()
})
