import React from "react"
import { TooltipDrawer } from "./tooltip-drawer"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

export const ForcedOpen = () => (
  <ThemeProvider style={{ padding: "11px 11px 80px" }}>
    <TooltipDrawer
      placeholder="placeholder"
      open={true} // force open
    >
      tooltip menu
    </TooltipDrawer>
  </ThemeProvider>
)
