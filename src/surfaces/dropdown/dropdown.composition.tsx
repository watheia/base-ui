import React from "react"
import { Dropdown } from "./dropdown"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

export const ForcedOpen = () => (
  <ThemeProvider style={{ padding: "11px 11px 70px" }}>
    <Dropdown
      placeholder="placeholder"
      open={true} // force open
    >
      tooltip menu
    </Dropdown>
  </ThemeProvider>
)

export const Uncontrolled = () => (
  <ThemeProvider style={{ padding: "11px 11px 70px" }}>
    <Dropdown placeholder="placeholder">tooltip menu</Dropdown>
  </ThemeProvider>
)
