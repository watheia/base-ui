/**
 * Optionally append prefix/postfix to a string.
 * @returns affixed string, or undefined if target is empty or undefined
 */

export function affix(prefix = "", str?: string | null | undefined, suffix = "") {
  // cast fasly to false
  if (!!!str) return ""

  return `${prefix}${str}${suffix}`
}
