import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Button } from "./button"

export const CallToActionButton = ({ ...rest }) => {
  return (
    <ThemeProvider>
      <Button importance="cta" style={{ width: 120 }} {...rest}>
        Submit
      </Button>
    </ThemeProvider>
  )
}

export const MainButton = () => {
  return (
    <ThemeProvider>
      <Button importance="ghost" style={{ width: 120 }}>
        Update
      </Button>
    </ThemeProvider>
  )
}

export const LightButton = ({ ...rest }) => {
  return (
    <ThemeProvider>
      <Button style={{ width: 120 }} {...rest}>
        learn more
      </Button>
    </ThemeProvider>
  )
}

export const CallToActionLoading = () => {
  return (
    <ThemeProvider>
      <Button importance="cta" loading style={{ width: 120 }}>
        Submit
      </Button>
    </ThemeProvider>
  )
}

export const Loading = () => {
  return (
    <ThemeProvider>
      <Button importance="ghost" loading style={{ width: 120 }}>
        Update
      </Button>
    </ThemeProvider>
  )
}

export const LightButtonLoading = () => {
  return (
    <ThemeProvider>
      <Button loading style={{ width: 120 }}>
        Learn more
      </Button>
    </ThemeProvider>
  )
}

export const CallToActionButtonDisabled = ({ ...rest }) => {
  return (
    <ThemeProvider>
      <Button importance="cta" style={{ width: 120 }} {...rest} disabled>
        Submit
      </Button>
    </ThemeProvider>
  )
}

export const MainButtonDisabled = () => {
  return (
    <ThemeProvider>
      <Button importance="ghost" style={{ width: 120 }} disabled>
        Update
      </Button>
    </ThemeProvider>
  )
}

export const LightButtonDisabled = ({ ...rest }) => {
  return (
    <ThemeProvider>
      <Button style={{ width: 120 }} {...rest} disabled>
        learn more
      </Button>
    </ThemeProvider>
  )
}

const compositions = [
  CallToActionButton,
  MainButton,
  LightButton,
  CallToActionLoading,
  Loading,
  LightButtonLoading,
]
// @ts-ignore
compositions.map((comp) => (comp.canvas = { height: 90 }))
