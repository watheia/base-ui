import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Image } from "./image"

export const ImageExample = ({ ...rest }) => (
  <ThemeProvider>
    <Image
      src="homepage-bit/map.png"
      data-testid="test-img"
      alt="alt world"
      style={{ width: 500 }}
      {...rest}
    />
  </ThemeProvider>
)

ImageExample.canvas = {
  height: 250,
}
