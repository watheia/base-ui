import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { H1, H2, H3, H4, H5, H6 } from "./heading"

export const HeadingH1 = () => (
  <ThemeProvider>
    <H1>Title</H1>
  </ThemeProvider>
)

export const HeadingH2 = () => (
  <ThemeProvider>
    <H2>Title</H2>
  </ThemeProvider>
)

export const HeadingH3 = () => (
  <ThemeProvider>
    <H3>Title</H3>
  </ThemeProvider>
)

export const HeadingH4 = () => (
  <ThemeProvider>
    <H4>Title</H4>
  </ThemeProvider>
)

export const HeadingH5 = () => (
  <ThemeProvider>
    <H5>Title</H5>
  </ThemeProvider>
)

export const HeadingH6 = () => (
  <ThemeProvider>
    <H6>Title</H6>
  </ThemeProvider>
)

const compositions = [HeadingH1, HeadingH2, HeadingH3, HeadingH4, HeadingH5, HeadingH6]
// @ts-ignore
compositions.map((comp) => (comp.canvas = { height: 90 }))
