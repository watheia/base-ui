import React from "react"
import { DotsLoader } from "./dots-loader"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

export const BasicDotsLoader = () => (
  <ThemeProvider>
    <div style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
      <DotsLoader />
    </div>
  </ThemeProvider>
)
