import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Error } from "./error"

export const ErrorMessage = () => (
  <ThemeProvider>
    <div style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
      <Error>error message!</Error>
    </div>
  </ThemeProvider>
)
