import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { PossibleSizes } from "@watheia/base-ui.theme.sizes"
import { Paragraph } from "./paragraph"

export const Pelement = () => (
  <ThemeProvider>
    <Paragraph element="p">p element</Paragraph>
  </ThemeProvider>
)

export const PelementWithDifferentSize = () => (
  <ThemeProvider>
    <Paragraph size={PossibleSizes.xl}>p element with xl size</Paragraph>
  </ThemeProvider>
)

export const DivElement = () => (
  <ThemeProvider>
    <Paragraph element="div">div element</Paragraph>
  </ThemeProvider>
)

export const SpanElement = () => (
  <ThemeProvider>
    <Paragraph element="span">span element</Paragraph>
  </ThemeProvider>
)
