import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { TextSeparator } from "./text-separator"

export const TextSeparatorExample = () => (
  <ThemeProvider>
    <div style={{ minWidth: 200 }}>
      <TextSeparator>text</TextSeparator>
    </div>
  </ThemeProvider>
)
