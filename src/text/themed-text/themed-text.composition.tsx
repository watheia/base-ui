import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { ThemedText, themedText } from "./themed-text"

export const ThemedTextExample = () => (
  <ThemeProvider>
    <ThemedText>text</ThemedText>
  </ThemeProvider>
)

export const UsingThemedTextClassName = () => (
  <ThemeProvider>
    <span className={themedText}>text</span>
  </ThemeProvider>
)
