import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { MutedText, mutedText } from "./muted-text"

export const MutedTextExample = () => (
  <ThemeProvider>
    <MutedText>Muted text</MutedText>
  </ThemeProvider>
)

export const UsingMutedTextClassName = () => (
  <ThemeProvider>
    <span className={mutedText}>Muted text with class name</span>
  </ThemeProvider>
)
