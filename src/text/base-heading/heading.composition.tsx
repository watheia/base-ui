import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Heading } from "./heading"

export const H1DefaultHeading = () => (
  <ThemeProvider>
    <Heading>H1 Heading</Heading>
  </ThemeProvider>
)

export const H2Heading = () => (
  <ThemeProvider>
    <Heading element="h2">H2 Heading</Heading>
  </ThemeProvider>
)

export const H3Heading = () => (
  <ThemeProvider>
    <Heading element="h3">H3 Heading</Heading>
  </ThemeProvider>
)

export const H4Heading = () => (
  <ThemeProvider>
    <Heading element="h4">H4 Heading</Heading>
  </ThemeProvider>
)

export const H5Heading = () => (
  <ThemeProvider>
    <Heading element="h5">H5 Heading</Heading>
  </ThemeProvider>
)

export const H6Heading = () => (
  <ThemeProvider>
    <Heading element="h6">H6 Heading</Heading>
  </ThemeProvider>
)
