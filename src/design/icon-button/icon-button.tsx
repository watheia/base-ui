import React from "react"
import classNames from "classnames"
import Button, { ButtonProps } from "@watheia/base-ui.input.button"
import { BaseIcon } from "@watheia/base-ui.atoms.icon"
import styles from "./icon-button.module.scss"

export type IconButtonProps = {
  /**
   * icon name
   */
  icon?: string
  /**
   * icon override class
   */
  iconClass?: string
  /**
   * indicate button is on
   */
  active?: boolean
} & ButtonProps

/**
 *
 * Generic button that supports text, icon and integration of both
 */
export function IconButton({
  icon,
  className,
  children,
  iconClass,
  active,
  ...rest
}: IconButtonProps) {
  return (
    <Button
      {...rest}
      className={classNames(
        styles.iconButton,
        active && styles.active,
        icon && !children && styles.iconOnly,
        className
      )}
    >
      {icon && (
        <BaseIcon
          className={classNames(
            styles.icon,
            active && styles.active,
            children && styles.margin,
            iconClass
          )}
          of={`bitcon-${icon}`}
        />
      )}
      {children}
    </Button>
  )
}
