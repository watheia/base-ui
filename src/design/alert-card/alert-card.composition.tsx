import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { AlertCard } from "./alert-card"

export const InfoAlertCard = () => (
  <ThemeProvider>
    <AlertCard level="info" title="Info title">
      Content to be rendered
    </AlertCard>
  </ThemeProvider>
)

export const WarningAlertCard = () => (
  <ThemeProvider>
    <AlertCard level="warning" title="Warning title">
      Content to be rendered
    </AlertCard>
  </ThemeProvider>
)

export const ErrorAlertCard = () => (
  <ThemeProvider>
    <AlertCard level="error" title="Error title">
      Content to be rendered
    </AlertCard>
  </ThemeProvider>
)

export const SuccessAlertCard = () => (
  <ThemeProvider>
    <AlertCard level="success" title="Success title">
      Content to be rendered
    </AlertCard>
  </ThemeProvider>
)
