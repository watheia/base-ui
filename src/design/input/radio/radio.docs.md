---
description: A radio input.
labels: ["react", "typescript", "input", "switch", "radio"]
---

import { Radio } from './radio';

A radio component with all the native options, based on
[checkbox from base-ui](/watheia.base-ui/input/checkbox/label) scope.  
The size is based on `em` so we can change the size by changing the font-size.  
Basic example:

```js live
<Radio />
```

Different size example:

```js live
<Radio style={{ fontSize: 24 }} />
```
