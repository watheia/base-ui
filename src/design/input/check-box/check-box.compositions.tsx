import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

import { CheckBox } from "./check-box"

export const Uncontrolled = () => {
  return (
    <ThemeProvider>
      <div>
        <CheckBox /> <span>Option</span>
      </div>
    </ThemeProvider>
  )
}

export const Checked = () => {
  return (
    <ThemeProvider>
      <div>
        <CheckBox defaultChecked /> <span>Option</span>
      </div>
    </ThemeProvider>
  )
}

export const disabled = () => {
  return (
    <ThemeProvider>
      <div>
        <CheckBox disabled /> <span>Option</span>
      </div>
    </ThemeProvider>
  )
}

export const disabledChecked = () => {
  return (
    <ThemeProvider>
      <div>
        <CheckBox disabled defaultChecked /> <span>Option</span>
      </div>
    </ThemeProvider>
  )
}

export const CheckedLarge = () => {
  return (
    <ThemeProvider>
      <div style={{ fontSize: 46 }}>
        <CheckBox defaultChecked /> <span>Option</span>
      </div>
    </ThemeProvider>
  )
}

export const CheckedSmall = () => {
  return (
    <ThemeProvider>
      <div style={{ fontSize: 12 }}>
        <CheckBox defaultChecked /> <span>12px</span>
      </div>
    </ThemeProvider>
  )
}

export const CheckedExtraSmall = () => {
  return (
    <ThemeProvider>
      <div style={{ fontSize: 8 }}>
        <CheckBox defaultChecked /> <span>8px</span>
      </div>
    </ThemeProvider>
  )
}
