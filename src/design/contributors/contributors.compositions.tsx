import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { AccountTypes } from "@watheia/base-ui.design.avatar"

import { Contributors } from "./index"

const mockContributors = [
  {
    name: "Should Not Appear",
    accountType: AccountTypes.user,
    displayName: "shouldDisplayThis",
    profileImage: "https://static.bit.dev/harmony/support.svg",
  },
  {
    name: "Mark Mock No DisplayName",
    accountType: AccountTypes.user,
    profileImage: "https://static.bit.dev/harmony/github.svg",
  },
  {
    name: "Three's a crowd",
    accountType: AccountTypes.user,
  },
]

export const SingleContributorExample = () => {
  return (
    <ThemeProvider>
      <Contributors contributors={[mockContributors[0]]} timestamp={Date().toString()} />
    </ThemeProvider>
  )
}

export const DoubleContributorExample = () => {
  return (
    <ThemeProvider>
      <Contributors
        contributors={[mockContributors[0], mockContributors[1]]}
        timestamp={Date().toString()}
      />
    </ThemeProvider>
  )
}

export const TripleContributorExample = () => {
  return (
    <ThemeProvider>
      <Contributors contributors={mockContributors} timestamp={Date().toString()} />
    </ThemeProvider>
  )
}
