import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { TimeAgo } from "./time-ago"

export const YearsAgoWithTimestamp = () => {
  return (
    <ThemeProvider>
      <TimeAgo date={1607550179} />
    </ThemeProvider>
  )
}

export const MonthTimeAgo = () => {
  const date = new Date()
  return (
    <ThemeProvider>
      <TimeAgo date={new Date(date.setMonth(date.getMonth() - 1)).toString()} />
    </ThemeProvider>
  )
}

export const MonthsTimeAgo = () => {
  const date = new Date()
  return (
    <ThemeProvider>
      <TimeAgo date={new Date(date.setMonth(date.getMonth() - 10)).toString()} />
    </ThemeProvider>
  )
}

export const HourTimeAgo = () => {
  const date = new Date()
  return (
    <ThemeProvider>
      <TimeAgo date={new Date(date.setHours(date.getHours() - 1)).toString()} />
    </ThemeProvider>
  )
}

export const HoursTimeAgo = () => {
  const date = new Date()
  return (
    <ThemeProvider>
      <TimeAgo date={new Date(date.setHours(date.getHours() - 10)).toString()} />
    </ThemeProvider>
  )
}

export const CurrentTime = () => {
  return (
    <ThemeProvider>
      <TimeAgo date={new Date().toString()} />
    </ThemeProvider>
  )
}

export const CurrentTimeWithIsoDate = () => {
  return (
    <ThemeProvider>
      <TimeAgo date={new Date().toISOString()} />
    </ThemeProvider>
  )
}
