import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"

import { CliSnippet } from "./index"

const content = `this is some cli content\n which should be rendered properly as html`

export const CliSnippetExample = () => {
  return (
    <ThemeProvider>
      <CliSnippet content={content} />
    </ThemeProvider>
  )
}
