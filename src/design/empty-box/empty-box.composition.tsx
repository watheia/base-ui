import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { EmptyBox } from "./empty-box"

export const EmptyBoxExample = () => {
  return (
    <ThemeProvider>
      <EmptyBox
        title="title-test"
        link="https://link-target/"
        linkText="link-text"
        className="test-class"
        data-testid="target"
      />
    </ThemeProvider>
  )
}

export const EmptyBoxExampleWithLongText = () => {
  return (
    <ThemeProvider>
      <EmptyBox
        title="There are no compositions for this component."
        link="https://link-target/"
        linkText="Learn how to create compositions"
        className="test-class"
        data-testid="target"
      />
    </ThemeProvider>
  )
}
