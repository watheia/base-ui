import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { Ellipsis } from "./index"

const longName = "This is a really long name so you'll see the ellipsis"
const shortName = "Short"

export const LongString = () => {
  return (
    <ThemeProvider>
      <Ellipsis style={{ width: 100 }}>{longName}</Ellipsis>
    </ThemeProvider>
  )
}

export const ShortString = () => {
  return (
    <ThemeProvider>
      <Ellipsis style={{ width: 100 }}>{shortName}</Ellipsis>
    </ThemeProvider>
  )
}
