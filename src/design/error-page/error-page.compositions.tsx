import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { ErrorPage } from "./error-page"

export const Error404 = () => (
  <ThemeProvider>
    <ErrorPage code={404} title="This is a 404 error page" />
  </ThemeProvider>
)

export const Error500 = () => (
  <ThemeProvider>
    <ErrorPage code={500} title="This is a 500 error page title. So just a general error" />
  </ThemeProvider>
)

export const ErrorUnknown = () => (
  <ThemeProvider>
    <ErrorPage
      code={12345}
      title="This is what you get if there's no such error page image available"
    />
  </ThemeProvider>
)
