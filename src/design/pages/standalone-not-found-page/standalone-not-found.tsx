import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { NotFoundPage, NotFoundPageProps } from "@watheia/base-ui.design.pages.not-found"

export type StandaloneNotFoundProps = NotFoundPageProps

/** A 404 page with fonts included  */
export function StandaloneNotFoundPage() {
  return (
    <ThemeProvider>
      <NotFoundPage />
    </ThemeProvider>
  )
}
