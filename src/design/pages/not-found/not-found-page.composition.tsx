import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { NotFoundPage } from "./not-found-page"

export const NotFoundPageExample = () => {
  return (
    <ThemeProvider>
      <NotFoundPage />
    </ThemeProvider>
  )
}
