import React from "react"
import { ThemeProvider } from "@watheia/base-ui.theme.theme-provider"
import { OwnerAvatar } from "./owner-avatar"

const userAccount = {
  name: "defaultAccount",
  type: "user",
  profileImage: "https://static.bit.dev/harmony/github.svg",
}

export const OwnerAvatarExample = () => (
  <ThemeProvider>
    <OwnerAvatar size={32} account={userAccount} />
  </ThemeProvider>
)
