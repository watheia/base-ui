FROM gitpod/workspace-full

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

# Install custom tools, runtimes, etc.
# For example "bastet", a command-line tetris clone:
# RUN brew install bastet
#
# More information: https://www.gitpod.io/docs/config-docker/

ENV PATH=$PATH:$HOME/bin
RUN echo 'export PATH=$PATH:$HOME/bin' >> ~/.bashrc

# Upgrade base sys
RUN sudo apt-get update && \
  sudo apt-get upgrade -y && \
  sudo apt-get install bash graphviz git git-lfs -y

RUN npm install --global @teambit/bvm

# start bit dev server
COPY --chown=gitpod .bitmap .
COPY --chown=gitpod workspace.jsonc .
RUN export PATH=$PATH:$HOME/bin && \
  bvm install && \
  bit config set analytics_reporting false && \
  bit config set error_reporting false && \
  bit config set no_warnings true && \
  bit init --harmony

# Run import in own cache layer
RUN bit import

# CMD [ "bit", "--help" ]
